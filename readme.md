## Quake Log Parser

Parser para o arquivo de log games.log.

O arquivo games.log é gerado pelo servidor de quake 3 arena. Ele registra todas as informações dos jogos, quando um jogo começa, quando termina, quem matou quem, quem morreu pq caiu no vazio, quem morreu machucado, entre outros.

O parser é capaz de ler o arquivo, agrupar os dados de cada jogo, e em cada jogo deve coletar as informações de morte.

## Como usar

Tenha ruby instalado em sua máquina e execute o comando: `ruby generate_score.rb`

## Testes

Para rodar os testes execute: 

`bundle install`

`rspec`
