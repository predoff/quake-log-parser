require_relative './kill.rb'
require "json"

class Parser
  attr_reader :log

  def initialize(log)
    @log = log
  end

  def kills_grouped_by_game
    @kills_grouped_by_game ||= generate_kills
  end

  # Generate main_score.json and mode_score.json based on Parser.log
  def generate_score
    main_score = []
    mode_score = []

    kills_grouped_by_game.each do |key, kills|
      main_score.push generate_main_score(kills, key)
      mode_score.push generate_mode_score(kills, key)
    end

    save_json main_score, "main_score.json"
    save_json mode_score, "mode_score.json"
  end

  # Generate main score hash
  #
  # game_1: {
  #   total_kills: 45;
  #   players: ["Dono da bola", "Isgalamido", "Zeh"]
  #   kills: {
  #     "Dono da bola": 5,
  #     "Isgalamido": 18,
  #     "Zeh": 20
  #   }
  # }
  def generate_main_score(kills, game_id)
    players = list_players(kills)
    kill_score = get_score(kills, players)

    Hash[game_id,
      {
        total_kills: kills.count,
        players: list_players(kills),
        kills: kill_score
      }
    ]
  end

  # Generate mode score hash
  # "game-1": {
  #   kills_by_means: {
  #     "MOD_SHOTGUN": 10,
  #     "MOD_RAILGUN": 2,
  #     "MOD_GAUNTLET": 1,
  #     "XXXX": N
  #   }
  # }
  def generate_mode_score(kills, game_id)
    mode_score = Hash[Kill.modes.map {|mode| [mode.to_s, 0]}]

    kills.each {|kill| mode_score[kill.mode] += 1 }

    # Sort by value DESC
    mode_score = Hash[mode_score.sort_by{|k, v| v}.reverse]

    Hash[game_id, {kills_by_means: mode_score}]
  end

  private
  def save_json(games_info, filepath)
    File.open(filepath, "w") do |f|
      f.write(JSON.pretty_generate(games_info))
    end
  end

  # For given arrays of kills and players return an Hash with count of kills to each player.
  def get_score(kills, players)
    kill_score = Hash[players.map {|player| [player, 0]}]

    kills.each do |kill|
      if kill.killer == "<world>"
        kill_score[kill.victim] -= 1
      else
        kill_score[kill.killer] += 1
      end
    end

    kill_score
  end

  # Given a list of kills and returns the unique players array from list.
  def list_players(kills)
    killers = kills.group_by(&:killer).keys
    victims = kills.group_by(&:victim).keys

    players = (killers | victims).sort

    # <world> should not be included on list
    players.delete('<world>')
    players
  end

  # For given log returns a Hash with list of Kills to each game found.
  #
  # @return [Hash]
  def generate_kills
    game_kills = {}

    list_game_logs.each_with_index do |game_log, index|
      game_id = "game_#{index + 1}"
      game_kills[game_id] = []
      kill_logs = list_kill_logs(game_log)

      kill_logs.each do |kill_log|
        kill = Kill.new(get_kill(kill_log),
                        get_victim(kill_log),
                        get_mode(kill_log))

        game_kills[game_id].push(kill)
      end
    end

    game_kills
  end

  def list_game_logs
    log.scan(/InitGame.*?ShutdownGame/m)
  end

  def list_kill_logs(game_log)
    game_log.scan(/Kill.*\n/)
  end

  def get_kill(kill_log)
    kill_log.match(/[0-9]: (.*) killed/)[1]
  end

  def get_victim(kill_log)
    kill_log.match(/killed (.*) by/)[1]
  end

  def get_mode(kill_log)
    kill_log.match(/by (.*)\n/)[1]
  end
end
