require_relative './lib/parser.rb'

puts "--------- Opening log"
log = File.open('./quake.log').read

puts "--------- Generating log"
Parser.new(log).generate_score

puts "--------- Completed. Check: main_score.json and mode_score.json files"
