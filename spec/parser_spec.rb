require 'spec_helper'

describe Parser do
  let(:valid_log) { File.open('./quake.log').read }
  let(:parser) { Parser.new(valid_log) }

  describe '#list_game_logs' do
    subject { parser.send(:list_game_logs) }

    it "each game log should start with 'InitGame'" do
      subject.each { |game_log| expect(game_log.start_with? 'InitGame').to be true }
    end

    it "each game log should end with 'ShutdownGame'" do
      subject.each { |game_log| expect(game_log.end_with? 'ShutdownGame').to be true }
    end
  end

  describe '#list_kill_logs' do
    subject { parser.send(:list_kill_logs, parser.log) }

    it "each kill log should start with 'Kill'" do
      subject.each { |kill_log| expect(kill_log.start_with? 'Kill').to be true }
    end

    it "each kill log should end with new line" do
      subject.each { |kill_log| expect(kill_log.end_with? "\n").to be true }
    end
  end

  describe "getting kill information from kill log" do
    let(:kill_log) { "22:06 Kill: 2 3 7: Isgalamido killed Mocinha by MOD_ROCKET_SPLASH\n" }

    describe "#get_killer" do
      it "should return string between '{number}: ' and ' killed'" do
        killer = parser.send(:get_kill, kill_log)
        expect(killer).to eq "Isgalamido"
      end
    end

    describe "#get_victim" do
      it "should return string between 'killed ' and ' by'" do
        victim = parser.send(:get_victim, kill_log)
        expect(victim).to eq "Mocinha"
      end
    end

    describe "#get_mode" do
      it "should return string between 'by ' and new line" do
        mode = parser.send(:get_mode, kill_log)
        expect(mode).to eq "MOD_ROCKET_SPLASH"
      end
    end
  end
end
